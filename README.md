# Lauritz Thomsen's cryptographic keys

These are my cryptographic keys, used to secure my communications and
prove the authenticity of the messages I send. All these keys are signed
by both my offline OpenPGP key and offline Signify key (with the exception
that they don't sign themselves, instead being signed by the general key).

You can read more about my cryptographic keys at:
- https://lauritzt.dk/keys

You can read more about contacting me securely at:
- https://lauritzt.dk/contact/security.html

All of these files are placed at (and can be updated from):
- https://lauritzt.dk/keys/
- https://gitlab.com/lauritzt.dk/keys/
- kbfs/public/lauritzt/keys/

The filenames have the format:
- For keys: `lt-[keytype]-[description].[ext]`.
- For Signify signatures: `[key-filename].sig`.
- For OpenPGP signatures: `[key-filename].asc`.

Definitions of keytypes:
- `openpgp`: Keys for OpenPGP (RFC 4880), used by PGP and GnuPG.
- `signify`: Keys for Signify, the OpenBSD signature utility, and Minisign.
- `keybase`: Lists of keys for Keybase (keybase.io).
- `ssh`: Consolidated lists of SSH (Secure Shell) keys.

Defintions of descriptions:
- `general`: For most messages, used for most manual signatures.
- `offline`: Highly protected keys, mostly used to sign the other keys.
- `known_hosts`: List of host key fingerprints (mine and others).
- `authorized_keys`: List of keys used to authenticate myself.
